import styled from "styled-components";

export const Timeline = styled.div`
  position: relative;
  max-width: 1200px;
  
  /* height: 50vh; */
  margin: 0 auto;
  
  &:after {
    content: "";
    position: absolute;
    width: 6px;
    background-color: #5f4388;
    top: 35px;
    bottom: 30px;
    left: 18.6%;
    margin-left: -3px;
  }
  .container {
    padding: 10px 40px;
    position: relative;
    background-color: inherit;
    width: 100%;
    left: 19.2%;
  }

  /* The circles on the timeline */
  .container::after {
    content: "";
    position: absolute;
    width: 20px;
    height: 20px;
    background-color: #5f4388;
    border: 4px solid #5f4388;
    top: 27px;
    border-radius: 50%;
    z-index: 1;
  }

  /* Fix the circle for containers on the right side */
  .right::after {
    left: -16px;
  }

  /* The actual content */
  .content {
    width: 100%;
    /* padding: 20px 30px; */
    position: relative;
    border-radius: 6px;
    display: flex;
    align-items: center;

    h3 {
      margin-right: 15px;
    }
  }

  /* Media queries - Responsive timeline on screens less than 600px wide */
  @media (max-width: 800px) {
    .container::after {
      top: 40%;
    }

    .right:after {
      left: -5.7%;
    }
    &:after {
      left: 17.5%;
    }
  }
`;
