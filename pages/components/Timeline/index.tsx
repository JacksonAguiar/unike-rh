import React from "react";

import { Timeline } from "./styles";

const TimelineComponent: React.FC = () => {
  return (
      //max 6
    <Timeline>
      <div className="container right">
        <div className="content">
          <h3>10:00</h3>
          <p>
          Café da manhã com o CEO
          </p>
        </div>
      </div>
      <div className="container right">
        <div className="content">
          <h3>11:00</h3>
          <p>Bate Papo com o RH</p>
        </div>
      </div>
      <div className="container right">
        <div className="content">
          <h3>12:00</h3>
          <p>Almoço com os UNIKOS</p>
        </div>
      </div>
      <div className="container right">
        <div className="content">
          <h3>13:30</h3>
          <p>Conhecendo o Time</p>
        </div>
      </div>
      <div className="container right">
        <div className="content">
          <h3>14:00</h3>
          <p>VAMOS NESSA!</p>
        </div>
      </div>
    </Timeline>
  );
};

export default TimelineComponent;
