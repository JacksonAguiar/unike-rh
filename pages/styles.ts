import styled from "styled-components";
export const Container = styled.div`
  padding: 0 2rem;
  height: 100vh;
  display: flex;
  flex-direction: column;

  header {
    flex: 0.8;
    display: flex;
    flex-direction: column;
    align-items: center;
    .toolbar {
      height: 100%;
      width: 100%;
      position: absolute;
    }
    .background{
      background-image: url("/static/background.jpg");
      position: relative;
      height: 100%;
      width: 100%;
    }
    .img {
      width: 150px;
      height: 150px;
      background: #eaeaea;
      border: 5px solid #fff;
    }
  }
  .content {
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    width: fit-content;
    position: relative;
    top: 13%;
    margin: auto;
    right: 0;
    left: 0;
    h2 {
      margin: 10px 15px;
    }
  }
  main {
    padding: 4rem 0;
    flex: 2;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    /* overflow: none; */
    .timeline_content {
      width: 80%;
      height: 100%;
      display: flex;
      align-items: center;
    }
  }
  footer {
    display: flex;
    padding: 2rem 0;
    border-top: 1px solid #eaeaea;
    justify-content: center;
    align-items: center;
   
  }

  @media (max-height: 800px) {
    .content {
      top: 5%;
    } 
  }
`;
