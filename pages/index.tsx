import type { NextPage } from "next";
import Image from "next/image";
import useSWR from "swr";

import Head from "next/head";
import Timeline from "./components/Timeline";
import { Container } from "./styles";
import { useEffect, useState } from "react";
import axios from "axios";

const Home: NextPage = () => {
  var image: string = "";
  const [data, setData] = useState<any>();
  const cpf = ["123", "321"];
  useEffect(() => {
    const id = setInterval(() => {
      axios
        .post("http://localhost:8080/onboard", cpf)
        .then((response) => setData(response.data));
    }, 5000);
    return () => clearInterval(id);
  }, []);

  if (data)
    image = data === cpf[0] ? "/static/Flavio.png" : "/static/Gustavo.png";
  if (!data) return <h1>Aguardando</h1>;

  return <Image layout="fill" objectFit="fill" alt="" src={image} />;
};
//   return (
//     <Container>
//       <header>
//         <div className={"background"}>
//           <Image
//             layout='fill'
//             objectFit='cover'
//             alt=""
//             src={"/static/background.jpg"}
//           />
//         </div>
//         <div className={"toolbar"}>
//           <div className={"content"}>
//             <div className={"img"}>
//               <Image
//                 width={200}
//                 height={200}
//                 objectFit="cover"
//                 src={
//                   "https://noticias.buscavoluntaria.com.br/wp-content/uploads/2019/04/@matt_damon_official.png"
//                 }
//                 alt="user"
//               />
//             </div>
//             <h2>Bem vindo renato</h2>
//           </div>
//         </div>
//       </header>
//       <main>
//         <div className={"timeline_content"}>
//           <Timeline />
//         </div>
//       </main>

//       <footer>
//         <Image
//           width={150}
//           height={50}
//           objectFit="cover"
//           alt=""
//           src={"/static/logo.png"}
//         />
//       </footer>
//     </Container>
//   );
// };

export default Home;
