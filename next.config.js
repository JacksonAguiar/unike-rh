/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['noticias.buscavoluntaria.com.br'],
  }
};
